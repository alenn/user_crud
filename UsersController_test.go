package main

import (
	"encoding/json"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"user_crud/db"
	"user_crud/routes"
)

func TestUsersController_Index(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Error(err.Error())
	}
	defer mockDB.Close()

	columns := []string{"id", "username", "name", "birth_date", "hire_date"}
	mock.ExpectPrepare("SELECT (.+) FROM users").ExpectQuery().
		WillReturnRows(sqlmock.NewRows(columns).FromCSVString("2,alenn,Alen,20.07.1992,"))

	db.InitDB()
	db.DB = mockDB

	req, err := http.NewRequest("GET", "/api/users", nil)
	if err != nil {
		t.Error(err.Error())
	}
	req.Header.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcnkiOjE1NDkxMDk4NTAsInVzZXJfaWQiOjJ9.IuVm7XuohcpVgcpJeM9kDDysakPH4iZn7VQCfnQYdaw")

	rw := httptest.NewRecorder()
	r := routes.InitRouter()

	r.ServeHTTP(rw, req)

	assert.Equal(t, rw.Code, 200)
	assert.Equal(t, rw.Body.String(), `{"data":[{"id":2,"username":"alenn","name":"Alen","birth_date":"20.07.1992","hire_date":""}],"status":"200"}`)
}

func TestUsersController_Show(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Error(err.Error())
	}
	defer mockDB.Close()

	columns := []string{"id", "username", "name", "birth_date", "hire_date"}
	mock.ExpectPrepare("SELECT (.+) FROM users").ExpectQuery().
		WillReturnRows(sqlmock.NewRows(columns).FromCSVString("2,alenn,Alen,20.07.1992,"))

	db.InitDB()
	db.DB = mockDB

	req, err := http.NewRequest("GET", "/api/users/1", nil)
	if err != nil {
		t.Error(err.Error())
	}
	req.Header.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcnkiOjE1NDkxMDk4NTAsInVzZXJfaWQiOjJ9.IuVm7XuohcpVgcpJeM9kDDysakPH4iZn7VQCfnQYdaw")

	rw := httptest.NewRecorder()
	r := routes.InitRouter()

	r.ServeHTTP(rw, req)

	assert.Equal(t, rw.Code, 200)
	assert.Equal(t, rw.Body.String(), `{"data":{"id":2,"username":"alenn","name":"Alen","birth_date":"20.07.1992","hire_date":""},"status":"200"}`)
}

func TestUsersController_Store(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Error(err.Error())
	}
	defer mockDB.Close()

	mock.ExpectPrepare("SELECT count\\(id\\) as c FROM users WHERE username = \\?")
	mock.ExpectQuery("SELECT count\\(id\\)").WithArgs("alenn").
		WillReturnRows(sqlmock.NewRows([]string{"c"}).FromCSVString("0"))

	mock.ExpectPrepare("INSERT INTO users (.+)")
	mock.ExpectExec("INSERT INTO users (.+)").
		WithArgs("alenn", AnyHash{}, "Alen", "20.07.1992", "").
		WillReturnResult(sqlmock.NewResult(1, 1))

	db.InitDB()
	db.DB = mockDB

	data := map[string]interface{}{
		"username": "alenn",
		"password": "abc123",
		"name": "Alen",
		"birthDate": "20.07.1992",
		"hireDate": "",
	}
	d, err := json.Marshal(data)
	if err != nil {
		t.Error(err.Error())
	}
	body := strings.NewReader(string(d))

	req, err := http.NewRequest("POST", "/api/users", body)
	if err != nil {
		t.Error(err.Error())
	}
	req.Header.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcnkiOjE1NDkxMDk4NTAsInVzZXJfaWQiOjJ9.IuVm7XuohcpVgcpJeM9kDDysakPH4iZn7VQCfnQYdaw")

	rw := httptest.NewRecorder()
	r := routes.InitRouter()

	r.ServeHTTP(rw, req)

	assert.Equal(t, rw.Code, 200)
	output := struct {
		Data string `json:"data"`
		Status string `json:"status"`
	}{}

	err = json.Unmarshal([]byte(rw.Body.String()), &output)
	if err != nil {
		t.Error(err.Error())
	}
	assert.Equal(t, "200", output.Status)
}

func TestUsersController_Update(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Error(err.Error())
	}
	defer mockDB.Close()

	mock.ExpectPrepare("SELECT count\\(id\\)")
	mock.ExpectQuery("SELECT count\\(id\\)").WithArgs("alen123", "1").
		WillReturnRows(sqlmock.NewRows([]string{"c"}).FromCSVString("0"))

	mock.ExpectPrepare("UPDATE users")
	mock.ExpectExec("UPDATE users").
		WithArgs("alen123", AnyHash{}, "Alen", "20.07.1992", "", "1").
		WillReturnResult(sqlmock.NewResult(1, 1))

	db.InitDB()
	db.DB = mockDB

	data := map[string]interface{}{
		"username": "alen123",
		"password": "abc123",
		"name": "Alen",
		"birthDate": "20.07.1992",
		"hireDate": "",
	}
	d, err := json.Marshal(data)
	if err != nil {
		t.Error(err.Error())
	}
	body := strings.NewReader(string(d))

	req, err := http.NewRequest("PUT", "/api/users/1", body)
	if err != nil {
		t.Error(err.Error())
	}
	req.Header.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcnkiOjE1NDkxMDk4NTAsInVzZXJfaWQiOjJ9.IuVm7XuohcpVgcpJeM9kDDysakPH4iZn7VQCfnQYdaw")

	rw := httptest.NewRecorder()
	r := routes.InitRouter()

	r.ServeHTTP(rw, req)

	assert.Equal(t, rw.Code, 200)
	output := struct {
		Data string `json:"data"`
		Status string `json:"status"`
	}{}

	err = json.Unmarshal([]byte(rw.Body.String()), &output)
	if err != nil {
		t.Error(err.Error())
	}
	assert.Equal(t, "200", output.Status)
}

func TestUsersController_Destroy(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Error(err.Error())
	}
	defer mockDB.Close()

	mock.ExpectExec("DELETE FROM users").WithArgs("1").WillReturnResult(sqlmock.NewResult(0, 1))

	db.InitDB()
	db.DB = mockDB

	req, err := http.NewRequest("DELETE", "/api/users/1", nil)
	if err != nil {
		t.Error(err.Error())
	}
	req.Header.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcnkiOjE1NDkxMDk4NTAsInVzZXJfaWQiOjJ9.IuVm7XuohcpVgcpJeM9kDDysakPH4iZn7VQCfnQYdaw")

	rw := httptest.NewRecorder()
	r := routes.InitRouter()

	r.ServeHTTP(rw, req)

	assert.Equal(t, rw.Code, 200)
	output := struct {
		Data string `json:"data"`
		Status string `json:"status"`
	}{}

	err = json.Unmarshal([]byte(rw.Body.String()), &output)
	if err != nil {
		t.Error(err.Error())
	}
	assert.Equal(t, "200", output.Status)
}

func TestUsersController_MissingAuthenticationHeader(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/users", nil)
	if err != nil {
		t.Error(err.Error())
	}

	rw := httptest.NewRecorder()
	r := routes.InitRouter()

	r.ServeHTTP(rw, req)

	assert.Equal(t, rw.Code, 200)
	output := struct {
		Data string `json:"data"`
		Status string `json:"status"`
	}{}

	err = json.Unmarshal([]byte(rw.Body.String()), &output)
	if err != nil {
		t.Error(err.Error())
	}
	assert.Equal(t, "403", output.Status)
	assert.Equal(t, "Authorization header missing", output.Data)
}

func TestUsersController_WrongToken(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/users", nil)
	if err != nil {
		t.Error(err.Error())
	}
	req.Header.Add("Authorization", "Bearer somewrongtoken")
	rw := httptest.NewRecorder()
	r := routes.InitRouter()

	r.ServeHTTP(rw, req)

	assert.Equal(t, rw.Code, 200)
	output := struct {
		Data string `json:"data"`
		Status string `json:"status"`
	}{}

	err = json.Unmarshal([]byte(rw.Body.String()), &output)
	if err != nil {
		t.Error(err.Error())
	}
	assert.Equal(t, "500", output.Status)
	assert.Equal(t, "token contains an invalid number of segments", output.Data)
}