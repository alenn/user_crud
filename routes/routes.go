package routes

import (
	"github.com/gorilla/mux"
	"user_crud/controllers"
	"user_crud/middleware"
)

func InitRouter() *mux.Router {
	router := mux.NewRouter()

	authController := new(controllers.AuthController)
	router.HandleFunc("/api/auth/login", authController.Login).Methods("POST")
	router.HandleFunc("/api/auth/register", authController.Register).Methods("POST")

	usersController := new(controllers.UsersController)
	router.HandleFunc("/api/users", middleware.JWTMiddleware(usersController.Index)).Methods("GET")
	router.HandleFunc("/api/users/{id}", middleware.JWTMiddleware(usersController.Show)).Methods("GET")
	router.HandleFunc("/api/users", middleware.JWTMiddleware(usersController.Store)).Methods("POST")
	router.HandleFunc("/api/users/{id}", middleware.JWTMiddleware(usersController.Update)).Methods("PUT")
	router.HandleFunc("/api/users/{id}", middleware.JWTMiddleware(usersController.Destroy)).Methods("DELETE")

	return router
}