package middleware

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"strings"
	"time"
	"user_crud/controllers"
)

func JWTMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		apiController := new(controllers.APIController)

		header := r.Header.Get("Authorization")
		if header == "" {
			w.Write(apiController.ReturnError("403", "Authorization header missing"))
			return
		}

		token := ""

		temp := strings.Split(header, " ")
		if len(temp) > 1 {
			token = strings.TrimSpace(temp[1])
		}

		if token == "" {
			w.Write(apiController.ReturnError("403", "Invalid authorization code"))
			return
		}

		t, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			return []byte("somesecretstring"), nil
		})

		if err != nil {
			w.Write(apiController.ReturnError("500", err.Error()))
			return
		}

		if claims, ok := t.Claims.(jwt.MapClaims); ok && t.Valid {
			// claims["user_id"]
			expiry := time.Unix(int64(claims["expiry"].(float64)), 0)
			if time.Until(expiry).Seconds() <= 0 {
				w.Write(apiController.ReturnError("403", "Token expired"))
				return
			}
		} else {
			w.Write(apiController.ReturnNotAuthorized())
			return
		}

		next.ServeHTTP(w, r)
	})
}