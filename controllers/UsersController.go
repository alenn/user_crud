package controllers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"time"
	"user_crud/db"
	"user_crud/models"
)

type UsersController struct {
	APIController
}

// Index lists all entries
func (this *UsersController) Index(w http.ResponseWriter, r *http.Request) {
	var users []models.User

	stmt, err := db.DB.Prepare("SELECT id, username, name, birth_date, hire_date FROM users")
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	for rows.Next() {
		var tempUser = models.User{}
		err := rows.Scan(&tempUser.ID, &tempUser.Username, &tempUser.Name, &tempUser.BirthDate, &tempUser.HireDate)
		if err != nil {
			w.Write(this.ReturnError("500", err.Error()))
		}

		users = append(users, tempUser)
	}

	w.Write(this.ReturnSuccess(users))
}

// Show lists single entry
func (this *UsersController) Show(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	user_id := vars["id"]

	var user models.User

	stmt, err := db.DB.Prepare("SELECT id, username, name, birth_date, hire_date FROM users WHERE id = ?")
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}
	defer stmt.Close()

	row := stmt.QueryRow(user_id)

	err = row.Scan(&user.ID, &user.Username, &user.Name, &user.BirthDate, &user.HireDate)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	w.Write(this.ReturnSuccess(user))
}

// Store creates new entry
func (this *UsersController) Store(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	input := Input{}

	err := decoder.Decode(&input)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	if input.Password == "" {
		w.Write(this.ReturnError("403", "Password is required"))
		return
	}

	if input.Username == "" {
		w.Write(this.ReturnError("403", "Username is required"))
		return
	} else {
		stmt, err := db.DB.Prepare("SELECT count(id) as c FROM users WHERE username = ?")
		if err != nil {
			w.Write(this.ReturnError("500", err.Error()))
			return
		}
		defer stmt.Close()

		count := 0

		row := stmt.QueryRow(input.Username)
		err = row.Scan(&count)
		if err != nil {
			w.Write(this.ReturnError("500", err.Error()))
			return
		}

		if count > 0 {
			w.Write(this.ReturnError("403", "Username is taken"))
			return
		}
	}

	if input.Name == "" {
		w.Write(this.ReturnError("403", "Name is required"))
		return
	}

	if input.BirthDate == "" {
		w.Write(this.ReturnError("403", "BirthDate is required"))
		return
	} else {
		var err error
		_, err = time.Parse("02.01.2006", input.BirthDate)
		if err != nil {
			w.Write(this.ReturnError("500", "BirthDate format should be d.m.Y"))
			return
		}
	}

	if input.HireDate != "" {
		var err error
		_, err = time.Parse("02.01.2006", input.HireDate)
		if err != nil {
			w.Write(this.ReturnError("500", "HireDate format should be d.m.Y"))
			return
		}
	}

	hashPassword, err := bcrypt.GenerateFromPassword([]byte(input.Password), 10)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	stmt, err := db.DB.Prepare("INSERT INTO users (username, password, name, birth_date, hire_date) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	_, err = stmt.Exec(input.Username, string(hashPassword), input.Name, input.BirthDate, input.HireDate)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	w.Write(this.ReturnSuccess(nil))
}

// Update updates single entry
func (this *UsersController) Update(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	user_id := vars["id"]

	decoder := json.NewDecoder(r.Body)

	input := Input{}

	err := decoder.Decode(&input)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	if input.Password == "" {
		w.Write(this.ReturnError("403", "Password is required"))
		return
	}

	if input.Username == "" {
		w.Write(this.ReturnError("403", "Username is required"))
		return
	} else {
		stmt, err := db.DB.Prepare("SELECT count(id) as c FROM users WHERE username = ? AND id != ?")
		if err != nil {
			w.Write(this.ReturnError("500", err.Error()))
			return
		}
		defer stmt.Close()

		count := 0

		row := stmt.QueryRow(input.Username, user_id)
		err = row.Scan(&count)
		if err != nil {
			w.Write(this.ReturnError("500", err.Error()))
			return
		}

		if count > 0 {
			w.Write(this.ReturnError("403", "Username is taken"))
			return
		}
	}

	if input.Name == "" {
		w.Write(this.ReturnError("403", "Name is required"))
		return
	}

	if input.BirthDate == "" {
		w.Write(this.ReturnError("403", "BirthDate is required"))
		return
	} else {
		var err error
		_, err = time.Parse("02.01.2006", input.BirthDate)
		if err != nil {
			w.Write(this.ReturnError("500", "BirthDate format should be d.m.Y"))
			return
		}
	}

	if input.HireDate != "" {
		var err error
		_, err = time.Parse("02.01.2006", input.HireDate)
		if err != nil {
			w.Write(this.ReturnError("500", "HireDate format should be d.m.Y"))
			return
		}
	}

	hashPassword, err := bcrypt.GenerateFromPassword([]byte(input.Password), 10)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	stmt, err := db.DB.Prepare("UPDATE users SET username = ?, password = ?, name = ?, birth_date = ?, hire_date = ? WHERE id = ?")
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	_, err = stmt.Exec(input.Username, string(hashPassword), input.Name, input.BirthDate, input.HireDate, user_id)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	w.Write(this.ReturnSuccess(nil))
}

// Destroy deletes single entry
func (this *UsersController) Destroy(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	user_id := vars["id"]
	_, err := db.DB.Exec("DELETE FROM users WHERE id = ?", user_id)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	w.Write(this.ReturnSuccess(nil))
}

