package controllers

import (
	"encoding/json"
)

const SYSTEM_ERROR = "500"
const SUCCESS = "200"
const NOT_AUTHORIZED = "403"

type APIController struct{}

// ReturnSuccess returns success response
func (this *APIController) ReturnSuccess(data interface{}) []byte {
	response := make(map[string]interface{})
	response["status"] = SUCCESS
	response["data"] = data

	jsonData, err := json.Marshal(response)
	if err == nil {
		return jsonData
	}

	return []byte(err.Error())
}

// ReturnError returns error response
func (this *APIController) ReturnError(code string, data interface{}) []byte {
	response := make(map[string]interface{})
	if code == "" {
		code = SYSTEM_ERROR
	}
	response["status"] = code
	response["data"] = data

	jsonData, err := json.Marshal(response)
	if err == nil {
		return jsonData
	}

	return []byte(err.Error())
}

// ReturnNotAuthorized returns not authorized response
func (this *APIController) ReturnNotAuthorized() []byte {
	response := make(map[string]interface{})
	response["status"] = NOT_AUTHORIZED
	response["data"] = "Not authorized"

	jsonData, err := json.Marshal(response)
	if err == nil {
		return jsonData
	}

	return []byte(err.Error())
}