package controllers

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"time"
	"user_crud/db"
	"user_crud/models"
)

type Input struct {
	Username             string `json:"username"`
	Password             string `json:"password"`
	PasswordConfirmation string `json:"passwordConfirmation"`
	Name                 string `json:"name"`
	BirthDate            string `json:"birthDate"`
	HireDate             string `json:"hireDate"`
}

type AuthController struct {
	APIController
}

// Login handles user authentication
func (this *AuthController) Login(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	input := Input{}

	err := decoder.Decode(&input)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	var user models.User

	stmt, err := db.DB.Prepare("SELECT id, username, password, name, birth_date, hire_date FROM users where username = ?")
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}
	defer stmt.Close()

	row := stmt.QueryRow(input.Username)
	if err != nil {
		w.Write(this.ReturnError("404", "User not found"))
		return
	}

	err = row.Scan(&user.ID, &user.Username, &user.Password, &user.Name, &user.BirthDate, &user.HireDate)
	if err != nil || user.Username == "" {
		w.Write(this.ReturnError("404", "User not found"))
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(input.Password))
	if err != nil {
		w.Write(this.ReturnError("403", "Credentials not correct"))
		return
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user_id": user.ID,
		"expiry": time.Now().Add(time.Hour * 24 * 7).Unix(),
	})

	tokenString, err := token.SignedString([]byte("somesecretstring"))
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
	}

	w.Write(this.ReturnSuccess(tokenString))
}

// Register handles user registration
func (this *AuthController) Register(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	input := Input{}

	err := decoder.Decode(&input)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	if input.Password == "" || input.Password != input.PasswordConfirmation {
		w.Write(this.ReturnError("403", "Passwords do not match"))
		return
	}

	if input.Username == "" {
		w.Write(this.ReturnError("403", "Username is required"))
		return
	} else {
		stmt, err := db.DB.Prepare("SELECT count(id) as c FROM users WHERE username = ?")
		if err != nil {
			w.Write(this.ReturnError("500", err.Error()))
			return
		}
		defer stmt.Close()

		count := 0

		row := stmt.QueryRow(input.Username)
		err = row.Scan(&count)
		if err != nil {
			w.Write(this.ReturnError("500", err.Error()))
			return
		}

		if count > 0 {
			w.Write(this.ReturnError("403", "Username is taken"))
			return
		}
	}

	if input.Name == "" {
		w.Write(this.ReturnError("403", "Name is required"))
		return
	}

	if input.BirthDate == "" {
		w.Write(this.ReturnError("403", "BirthDate is required"))
		return
	} else {
		var err error
		_, err = time.Parse("02.01.2006", input.BirthDate)
		if err != nil {
			w.Write(this.ReturnError("500", "BirthDate format should be d.m.Y"))
			return
		}
	}

	if input.HireDate != "" {
		var err error
		_, err = time.Parse("02.01.2006", input.HireDate)
		if err != nil {
			w.Write(this.ReturnError("500", "HireDate format should be d.m.Y"))
			return
		}
	}

	hashPassword, err := bcrypt.GenerateFromPassword([]byte(input.Password), 10)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	stmt, err := db.DB.Prepare("INSERT INTO users (username, password, name, birth_date, hire_date) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	_, err = stmt.Exec(input.Username, string(hashPassword), input.Name, input.BirthDate, input.HireDate)
	if err != nil {
		w.Write(this.ReturnError("500", err.Error()))
		return
	}

	w.Write(this.ReturnSuccess(nil))
}
