package main

import (
	"database/sql/driver"
	"log"
	"net/http"
	"user_crud/db"
	"user_crud/routes"
)

type AnyHash struct{}

// Match satisfies sqlmock.Argument interface
func (a AnyHash) Match(v driver.Value) bool {
	_, ok := v.(string)

	return ok
}

func main() {

	db.InitDB()

	router := routes.InitRouter()
	err := http.ListenAndServe(":8080", router)
	if err != nil {
		log.Fatal(err.Error())
	}

}