package main

import (
	"encoding/json"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/magiconair/properties/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"user_crud/db"
	"user_crud/routes"
)

func TestAuthController_LoginFailed(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Error(err.Error())
	}
	defer mockDB.Close()

	columns := []string{"id", "username", "password", "name", "birth_date", "hire_date"}
	mock.ExpectPrepare("SELECT id, username, password, name, birth_date, hire_date FROM users where username = ?")
	mock.ExpectQuery("SELECT id, username, password, name, birth_date, hire_date FROM users where username = ?").WithArgs("alenn").
		WillReturnRows(sqlmock.NewRows(columns).FromCSVString("2,alenn,abc,Alen,20.07.1992,"))

	db.InitDB()
	db.DB = mockDB

	data := map[string]interface{}{
		"username": "alenn",
		"password": "abc",
	}
	d, err := json.Marshal(data)
	if err != nil {
		t.Error(err.Error())
	}
	body := strings.NewReader(string(d))

	req, err := http.NewRequest("POST", "/api/auth/login", body)
	if err != nil {
		t.Error(err.Error())
	}

	rw := httptest.NewRecorder()
	r := routes.InitRouter()

	r.ServeHTTP(rw, req)

	assert.Equal(t, rw.Code, 200)
	assert.Equal(t, rw.Body.String(), `{"data":"Credentials not correct","status":"403"}`)
}

func TestAuthController_LoginSuccess(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Error(err.Error())
	}
	defer mockDB.Close()

	columns := []string{"id", "username", "password", "name", "birth_date", "hire_date"}
	mock.ExpectPrepare("SELECT id, username, password, name, birth_date, hire_date FROM users where username = ?")
	mock.ExpectQuery("SELECT id, username, password, name, birth_date, hire_date FROM users where username = ?").WithArgs("alenn").
		WillReturnRows(sqlmock.NewRows(columns).FromCSVString("2,alenn,$2a$10$rDnpCyHhusPMjYkpbIxV.uR4xDaPmcUNxVTOc3ZJGUQeVWj/YNGo6,Alen,20.07.1992,"))

	db.InitDB()
	db.DB = mockDB

	data := map[string]interface{}{
		"username": "alenn",
		"password": "abc123",
	}
	d, err := json.Marshal(data)
	if err != nil {
		t.Error(err.Error())
	}
	body := strings.NewReader(string(d))

	req, err := http.NewRequest("POST", "/api/auth/login", body)
	if err != nil {
		t.Error(err.Error())
	}

	rw := httptest.NewRecorder()
	r := routes.InitRouter()

	r.ServeHTTP(rw, req)

	assert.Equal(t, rw.Code, 200)
	output := struct {
		Data string `json:"data"`
		Status string `json:"status"`
	}{}

	err = json.Unmarshal([]byte(rw.Body.String()), &output)
	if err != nil {
		t.Error(err.Error())
	}
	assert.Equal(t, "200", output.Status)
}

func TestAuthController_RegisterFail(t *testing.T) {
	db.InitDB()

	data := map[string]interface{}{
		"username": "alenn",
		"password": "",
	}

	d, err := json.Marshal(data)
	if err != nil {
		t.Error(err.Error())
	}
	body := strings.NewReader(string(d))

	req, err := http.NewRequest("POST", "/api/auth/register", body)
	if err != nil {
		t.Error(err.Error())
	}

	rw := httptest.NewRecorder()
	r := routes.InitRouter()

	r.ServeHTTP(rw, req)

	assert.Equal(t, rw.Body.String(), `{"data":"Passwords do not match","status":"403"}`)
}

func TestAuthController_RegisterSuccess(t *testing.T) {

	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Error(err.Error())
	}
	defer mockDB.Close()

	mock.ExpectPrepare("SELECT count\\(id\\) as c FROM users WHERE username = \\?")
	mock.ExpectQuery("SELECT count\\(id\\)").WithArgs("alenn").
		WillReturnRows(sqlmock.NewRows([]string{"c"}).FromCSVString("0"))

	mock.ExpectPrepare("INSERT INTO users (.+)")
	mock.ExpectExec("INSERT INTO users (.+)").
		WithArgs("alenn", AnyHash{}, "Alen", "20.07.1992", "").
		WillReturnResult(sqlmock.NewResult(1, 1))

	db.InitDB()
	db.DB = mockDB

	data := map[string]interface{}{
		"username": "alenn",
		"password": "abc123",
		"passwordConfirmation": "abc123",
		"name": "Alen",
		"birthDate": "20.07.1992",
		"hireDate": "",
	}
	d, err := json.Marshal(data)
	if err != nil {
		t.Error(err.Error())
	}
	body := strings.NewReader(string(d))

	req, err := http.NewRequest("POST", "/api/auth/register", body)
	if err != nil {
		t.Error(err.Error())
	}

	rw := httptest.NewRecorder()
	r := routes.InitRouter()

	r.ServeHTTP(rw, req)

	assert.Equal(t, rw.Code, 200)
	output := struct {
		Data string `json:"data"`
		Status string `json:"status"`
	}{}

	err = json.Unmarshal([]byte(rw.Body.String()), &output)
	if err != nil {
		t.Error(err.Error())
	}
	assert.Equal(t, "200", output.Status)
}
