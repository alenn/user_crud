package models

type User struct {
	ID        int    `json:"id" db:"id"`
	Username  string `json:"username" db:"username"`
	Password  string `json:"-" db:"password"`
	Name      string `json:"name" db:"name"`
	BirthDate string `json:"birth_date" db:"birth_date"`
	HireDate  string `json:"hire_date" db:"hire_date"`
}
